docker_registry = "registry.gitlab.com/johnsondnz/container-images"
container_name  = "base"
default_user    = "generic"
default_uid     = "1000"
image_source    = "python:3-slim"
