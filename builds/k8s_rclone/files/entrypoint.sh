#!/bin/sh

if [[ -z ${START_CROND} ]]; then
  echo "==> START_CROND not defined, exiting..."
  exit 0
else
  echo "==> Print some useful stuff"
  #parse crontab
  /usr/bin/crontab /crontab

  #show crontab
  /usr/bin/crontab -l

  #rclone
  rclone version

  # start cron
  echo "==> START_CROND defined, starting..."
  /usr/sbin/crond -f -d 6
fi
