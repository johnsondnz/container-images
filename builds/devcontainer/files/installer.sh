#!/bin/bash

set -e

echo "==> What files copied over?"
ls -l /tmp/files

echo "==> Prepare system"
export DEBIAN_FRONTEND=noninteractive
apt-get update -qq
apt-get install --no-install-recommends -yqq curl wget software-properties-common gnupg2

echo "==> Install Hashicorp Repository"
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
gpg --no-default-keyring --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg --fingerprint
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/hashicorp.list

echo "==> Install terraform-docs"
curl -sSLo ./terraform-docs.tar.gz https://terraform-docs.io/dl/v0.15.0/terraform-docs-v0.15.0-$(uname)-amd64.tar.gz
tar -xzf terraform-docs.tar.gz
install -o root -g root -m 0755 terraform-docs /usr/bin/terraform-docs
rm ./terraform-docs.tar.gz

echo "==> Install GitLab Runner Respository"
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash

echo "==> Install any defined pip packages"
[ -s /tmp/files/pip-requirements.txt ] && echo "==> Install more pip packages" && pip3 install --quiet --no-cache-dir -r /tmp/files/pip-requirements.txt || echo "==> No additional pip packages to install"

echo "==> Install docker binary for command/control and local packer builds"
curl -fsSL https://get.docker.com | sh
usermod -aG docker generic

echo "==> Install apt packages"
apt-get update -qq
[ -s /tmp/files/apt-requirements.txt ] && echo "==> Install more apt packages" && xargs -a /tmp/files/apt-requirements.txt apt-get install --no-install-recommends -yqq || echo "==> No additional apt packages to install"

echo "==> Install kubectl"
curl -o /tmp/kubectl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
install -o root -g root -m 0755 /tmp/kubectl /usr/local/bin/kubectl
kubectl completion bash | tee /etc/bash_completion.d/kubectl > /dev/null
echo '[[ $commands[kubectl] ]] && source <(kubectl completion zsh)' >> ~/.zshrc 
rm /tmp/kubectl

echo "==> Install flux.io"
curl -s https://fluxcd.io/install.sh | bash

echo "==> Install helm"
curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash

echo "==> Install kustomize"
curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  | bash
install -m 755 kustomize /usr/local/bin/kustomize
rm kustomize

echo "==> Install kubeseal"
# Fetch the latest sealed-secrets version using GitHub API
KUBESEAL_VERSION=$(curl -s https://api.github.com/repos/bitnami-labs/sealed-secrets/tags | jq -r '.[0].name' | cut -c 2-)

# Check if the version was fetched successfully
if [ -z "$KUBESEAL_VERSION" ]; then
    echo "Failed to fetch the latest KUBESEAL_VERSION"
    exit 1
fi

echo "==> Install yamlfmt"
# Fetch the latest version using GitHub API
YAMLFMT_VERSION=$(curl -s https://api.github.com/repos/google/yamlfmt/tags | jq -r '.[0].name' | cut -c 2-)

# Check if the version was fetched successfully
if [ -z "$YAMLFMT_VERSION" ]; then
    echo "Failed to fetch the latest YAMLFMT_VERSION"
    exit 1
fi

wget "https://github.com/google/yamlfmt/releases/download/v${YAMLFMT_VERSION}/yamlfmt_${YAMLFMT_VERSION}_Linux_x86_64.tar.gz"
tar -xvzf yamlfmt_${YAMLFMT_VERSION}_Linux_x86_64.tar.gz yamlfmt
install -m 755 yamlfmt /usr/local/bin/yamlfmt
rm yamlfmt*


wget "https://github.com/bitnami-labs/sealed-secrets/releases/download/v${KUBESEAL_VERSION}/kubeseal-${KUBESEAL_VERSION}-linux-amd64.tar.gz"
tar -xvzf kubeseal-${KUBESEAL_VERSION}-linux-amd64.tar.gz kubeseal
install -m 755 kubeseal /usr/local/bin/kubeseal
rm kubeseal*




echo "==> Setup default shell for generic user"
chsh -s $(which zsh) generic

echo "==> Cleanup"
apt-get autoremove -yqq --purge
apt-get autoclean
apt-get clean
rm -rf /var/lib/apt/lists/*
