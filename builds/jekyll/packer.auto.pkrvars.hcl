docker_registry = "registry.gitlab.com/johnsondnz/container-images"
container_name  = "jekyll"
default_user    = "generic"
default_uid     = "1000"
image_source    = "registry.gitlab.com/johnsondnz/container-images/base:latest"
