#!/bin/bash

set -e

echo "==> Prepare system"
export DEBIAN_FRONTEND=noninteractive
apt-get update -qq
apt-get install --no-install-recommends -yqq curl wget software-properties-common gnupg2

echo "==> Install any defined pip packages"
[ -s /etc/pip-requirements.txt ] && echo "==> Install more pip packages" && pip3 install --quiet --no-cache-dir -r /etc/pip-requirements.txt || echo "==> No additional pip packages to install"

echo "==> Install apt packages"
[ -s /etc/apt-requirements.txt ] && echo "==> Install more apt packages" && xargs -a /etc/apt-requirements.txt apt-get install --no-install-recommends -yqq || echo "==> No additional apt packages to install"

echo "==> Cleanup"
apt-get autoremove -yqq --purge
apt-get autoclean
apt-get clean
rm -rf /var/lib/apt/lists/*
rm /etc/pip-requirements.txt
rm /etc/apt-requirements.txt
