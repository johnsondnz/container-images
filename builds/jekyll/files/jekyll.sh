#!/bin/bash

set -e

echo "==> Install Jekyll"
echo '# Install Ruby Gems to ~/gems' >> /home/generic/.zshrc
echo 'export GEM_HOME="$HOME/gems"' >> /home/generic/.zshrc
echo 'export PATH="$HOME/gems/bin:$PATH"' >> /home/generic/.zshrc
gem install jekyll bundler
